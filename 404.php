<?php get_header(); ?>

			<div id="content">

				<div id="inner-content" class="wrap clearfix">

					<div id="main" class="eightcol first clearfix" role="main">

						<article id="post-not-found" class="hentry clearfix">
							<div class="row">	
							<div class="col-md-1"></div>
							<div class="col-md-10" style="margin-top: 50px;margin-bottom: 30px;"> 
									<div class="mg_rocketLogin">
										<img src="<?php bloginfo('template_directory'); ?>/library/images/login_animate.png">
									</div>
									<header class="article-header">
										<h1>Oops, Post Not Found!</h1>
									</header>
									<section class="entry-content">
										<p>Uh Oh. Something is missing. Try double checking things.</p>
									</section>
									<footer class="article-footer">
											<p>You can back to <a href="<?php echo home_url(); ?>" title="back to homepage">Homepage here!</a> 
											<br/>Gempita</p>
									</footer>
							</div>
							<div class="col-md-1"> </div>
							</div>
						</article>

					</div>

				</div>

			</div>

<?php get_footer(); ?>
